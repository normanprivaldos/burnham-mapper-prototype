# Burnham Mapper Prototype

This contains the Frontend Implementation of the Burnham Snowflake API.

## Set-up
1. Apply the correct node version (`v10.23.0`), simply run the `nvm use` command to setup the correct node version.
2. Install necessary dependencies by running `npm install`.
3. Run the prototype by running `npm run watch`.

## Note
You need to run the Burnham Snowflake API server in order to make this prototype work. Go to the project folder of the API and run `npm run api-watch`