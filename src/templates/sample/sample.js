import React from 'react';

import SearchForm from '../../components/search-form/search-form';
import ResultList from '../../components/result-list/result-list';
import Map from '../../components/map/map';

import styles from './sample.scss';

const Sample = () => {
  return (
    <div className={styles.samplePage}>
      <div className={styles.samplePageSide}>
        <SearchForm />
        <ResultList />
      </div>
      <div className={styles.samplePageMap}>
        <Map />
      </div>
    </div>
  );
};

export default Sample;