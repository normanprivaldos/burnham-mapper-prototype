const Reducer = (state, action) => {
  switch (action.type) {
    case 'set_map': {
      return {
        ...state,
        map: action.payload,
      };
    }
    case 'set_places': {
      return {
        ...state,
        places: action.payload,
      };
    }
    case 'set_loading': {
      return {
        ...state,
        loading: action.payload,
      };
    }
    case 'set_data': {
      return {
        ...state,
        data: action.payload,
      };
    }
    default:
      return state;
  }
};

export default Reducer;
