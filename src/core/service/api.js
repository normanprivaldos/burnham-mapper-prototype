import axios from 'axios';

const url = 'http://localhost:3600/api';

const fetch = (endpoint, params = {}, callback) => {
  axios.get(`${url}${endpoint}`, { params }).then(callback);
};

export default fetch;
