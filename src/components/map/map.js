import React, { useState, useContext, useEffect } from 'react';

import { Context } from '../../core/store';

import styles from './map.scss';
import orangeMarker from '../../assets/icons/orange-marker.png';

const mapStyles = [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 65
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": "50"
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "30"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "40"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#ffff00"
            },
            {
                "lightness": -25
            },
            {
                "saturation": -97
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "lightness": -25
            },
            {
                "saturation": -100
            }
        ]
    }
];

const Map = () => {
  const [googleMapsLoaded, setGoogleMapsLoaded] = useState(false);

  const [state, dispatch] = useContext(Context);
  const { map: googleMap, data } = state;

  useEffect(() => {
    const url = 'https://maps.googleapis.com/maps/api/js?libraries=geometry,places&key=AIzaSyBMM2YY1EVxISo-APoFlqe6AYlM3PGPZbQ';

    if (!document.querySelector(`script[src="${url}"]`)) {
      const s = document.createElement('script');
      s.src = url;
      s.onload = () => setGoogleMapsLoaded(true);
      
      document.body.appendChild(s);
    }
  }, []);

  useEffect(() => {
    if (googleMapsLoaded && window.google && document.getElementById('map')) {
      const config = {
        center: { lat: 40.7185042, lng: -74.1314837 },
        zoom: 11,
        streetViewControl: true,
        mapTypeControl: false,
        fullscreenControl: false,
        styles: mapStyles,
      };

      const map = new window.google.maps.Map(document.getElementById('map'), config);
      const places = new window.google.maps.places.PlacesService(map);

      dispatch({
        type: 'set_map',
        payload: map,
      });

      dispatch({
        type: 'set_places',
        payload: places,
      });
    }
  }, [googleMapsLoaded]);

  useEffect(() => {
    if (data && googleMap && window.google && document.getElementById('map')) {
      data.data.forEach(item => {
        new window.google.maps.Marker({
          position: {
            lat: item.LATITUDE,
            lng: item.LONGITUDE,
          },
          icon: orangeMarker,
          map: googleMap,
        });
      });
    }
  }, [data]);

  return <div className={styles.map} id="map" />
};

export default Map;
