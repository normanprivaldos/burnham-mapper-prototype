import React, { useContext } from 'react';

import { Context } from '../../core/store';

import styles from './result-list.scss';

const ResultList = () => {

  const [state, dispatch] = useContext(Context);
  const { loading, data } = state;
  const count = data && data.meta && data.meta.total ? data.meta.total : 0;
  const list = data && data.data && data.data.length > 0 ? data.data : [];

  return (
    <div className={styles.resultList}>
      <div className={styles.resultListCounter}>
        {
          loading 
            ? 'Searching permits...'
            : `${ count } Results Found:`
        }
      </div>
      <div className={styles.resultListContainer}>
        {
          list.map(item => {
            return (
              <div key={item.PERMITID} className={styles.card}>
                <h3>Permit #{item.PERMITID}</h3>
                <div>{item.PERMITTYPE}</div>
              </div>
            )
          })
        }
      </div>
    </div>
  );
};

export default ResultList;
