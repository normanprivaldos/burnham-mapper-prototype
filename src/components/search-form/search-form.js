import React, { useState, useContext } from 'react';

import fetch from '../../core/service/api';

import { Context } from '../../core/store';

import Search from '../../assets/icons/magnify.svg';

import styles from './search-form.scss';

const SearchForm = () => {
  const [keyword, setKeyword] = useState('');
  
  const [state, dispatch] = useContext(Context);
  const { map, places } = state;

  const onSearch = () => {
    dispatch({
      type: 'set_loading',
      payload: true,
    });

    dispatch({
      type: 'set_data',
      payload: null,
    });

    places.findPlaceFromQuery({
      query: keyword,
      fields: ['place_id'],
    }, (results, status) => {
      if (status === 'OK' && results) {
        places.getDetails({
          placeId: results[0].place_id,
          fields: ['address_component', 'geometry', 'name'],
        }, (result, status) => {
          const center = {
            lat: result.geometry.location.lat(),
            lng: result.geometry.location.lng(),
          };

          map.setCenter(center);
          map.setZoom(12);

          const points = [
            map.getBounds().getSouthWest().lat(),
            map.getBounds().getSouthWest().lng(),
            map.getBounds().getNorthEast().lat(),
            map.getBounds().getNorthEast().lng(),
          ];

          fetch('/permits', {
            bounds: points.join(',')
          }, (res) => {
            if (res.statusText === 'OK') {
              dispatch({
                type: 'set_data',
                payload: res.data,
              });

              dispatch({
                type: 'set_loading',
                payload: false,
              });
            }
          });
        });
      }
    });
  };

  return (
    <div className={styles.searchForm}>
      <label htmlFor="search">Search Location:</label>
      <input 
        type="text" 
        name="search" 
        id="search" 
        placeholder="ex: Chicago" 
        onChange={e => setKeyword(e.target.value)}
      />
      <button type="button" aria-label="Search" onClick={onSearch}><Search /></button>
    </div>
  );
};

export default SearchForm;
