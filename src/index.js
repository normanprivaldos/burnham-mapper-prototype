import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Store from './core/store';

import Sample from './templates/sample/sample';

import './assets/scss/global.scss';

const App = () => (
  <Store>
    <Router>
      <Switch>
        <Route path="/">
          <Sample />
        </Route>
      </Switch>
    </Router>
  </Store>
);

render(<App/>, document.getElementById('root'));
